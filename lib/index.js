#!/usr/bin/env node

const fs = require('fs');
const path = require('path');

//command line arguments
//first argument is the directory you want to
//turn into a soundfont.
const sfPath = process.argv[2];
const drumParsing = process.argv[3] || false;

/**
 * Module dependencies.
 */
var crypto = require('crypto');

/**
 * Calculates the MD5 hash of a string.
 *
 * @param  {String} string - The string (or buffer).
 * @return {String}        - The MD5 hash.
 */
function md5(string) {
    return crypto.createHash('md5').update(string).digest('hex');
}

//only these files will be parsed
var acceptExt = [
	".wav", ".ogg", ".mp3", ".aiff", ".aif", ".flac"
];

var notesNumber = {}

notesNumber['C-2'] = 0
notesNumber['C#-2'] = 1
notesNumber['D-2'] = 2
notesNumber['D#-2'] = 3
notesNumber['E-2'] = 4
notesNumber['F-2'] = 5
notesNumber['F#-2'] = 6
notesNumber['G-2'] = 7
notesNumber['G#-2'] = 8
notesNumber['A-2'] = 9
notesNumber['A#-2'] = 10
notesNumber['B-2'] = 11
notesNumber['C-1'] = 12
notesNumber['C#-1'] = 13
notesNumber['D-1'] = 14
notesNumber['D#-1'] = 15
notesNumber['E-1'] = 16
notesNumber['F-1'] = 17
notesNumber['F#-1'] = 18
notesNumber['G-1'] = 19
notesNumber['G#-1'] = 20
notesNumber['A-1'] = 21
notesNumber['A#-1'] = 22
notesNumber['B-1'] = 23
notesNumber['C0'] = 24
notesNumber['C#0'] = 25
notesNumber['D0'] = 26
notesNumber['D#0'] = 27
notesNumber['E0'] = 28
notesNumber['F0'] = 29
notesNumber['F#0'] = 30
notesNumber['G0'] = 31
notesNumber['G#0'] = 32
notesNumber['A0'] = 33
notesNumber['A#0'] = 34
notesNumber['B0'] = 35
notesNumber['C1'] = 36
notesNumber['C#1'] = 37
notesNumber['D1'] = 38
notesNumber['D#1'] = 39
notesNumber['E1'] = 40
notesNumber['F1'] = 41
notesNumber['F#1'] = 42
notesNumber['G1'] = 43
notesNumber['G#1'] = 44
notesNumber['A1'] = 45
notesNumber['A#1'] = 46
notesNumber['B1'] = 47
notesNumber['C2'] = 48
notesNumber['C#2'] = 49
notesNumber['D2'] = 50
notesNumber['D#2'] = 51
notesNumber['E2'] = 52
notesNumber['F2'] = 53
notesNumber['F#2'] = 54
notesNumber['G2'] = 55
notesNumber['G#2'] = 56
notesNumber['A2'] = 57
notesNumber['A#2'] = 58
notesNumber['B2'] = 59
notesNumber['C3'] = 60
notesNumber['C#3'] = 61
notesNumber['D3'] = 62
notesNumber['D#3'] = 63
notesNumber['E3'] = 64
notesNumber['F3'] = 65
notesNumber['F#3'] = 66
notesNumber['G3'] = 67
notesNumber['G#3'] = 68
notesNumber['A3'] = 69
notesNumber['A#3'] = 70
notesNumber['B3'] = 71
notesNumber['C4'] = 72
notesNumber['C#4'] = 73
notesNumber['D4'] = 74
notesNumber['D#4'] = 75
notesNumber['E4'] = 76
notesNumber['F4'] = 77
notesNumber['F#4'] = 78
notesNumber['G4'] = 79
notesNumber['G#4'] = 80
notesNumber['A4'] = 81
notesNumber['A#4'] = 82
notesNumber['B4'] = 83
notesNumber['C5'] = 84
notesNumber['C#5'] = 85
notesNumber['D5'] = 86
notesNumber['D#5'] = 87
notesNumber['E5'] = 88
notesNumber['F5'] = 89
notesNumber['F#5'] = 90
notesNumber['G5'] = 91
notesNumber['G#5'] = 92
notesNumber['A5'] = 93
notesNumber['A#5'] = 94
notesNumber['B5'] = 95
notesNumber['C6'] = 96
notesNumber['C#6'] = 97
notesNumber['D6'] = 98
notesNumber['D#6'] = 99
notesNumber['E6'] = 100
notesNumber['F6'] = 101
notesNumber['F#6'] = 102
notesNumber['G6'] = 103
notesNumber['G#6'] = 104
notesNumber['A6'] = 105
notesNumber['A#6'] = 106
notesNumber['B6'] = 107
notesNumber['C7'] = 108
notesNumber['C#7'] = 109
notesNumber['D7'] = 110
notesNumber['D#7'] = 111
notesNumber['E7'] = 112
notesNumber['F7'] = 113
notesNumber['F#7'] = 114
notesNumber['G7'] = 115
notesNumber['G#7'] = 116
notesNumber['A7'] = 117
notesNumber['A#7'] = 118
notesNumber['B7'] = 119
notesNumber['C8'] = 120
notesNumber['C#8'] = 121
notesNumber['D8'] = 122
notesNumber['D#8'] = 123
notesNumber['E8'] = 124
notesNumber['F8'] = 125
notesNumber['F#8'] = 126
notesNumber['G8'] = 127

if (!drumParsing) {
	var noteExp = /(_)?[A-G]#?-?[0-8]/g;
	var velExp = /(_)?[A-G]#?-?[0-8](_)[\d+]/g;
} else {
	var noteExp = /^\w+/g;
	var velExp = /(_)?[A-G]#?-?[0-8](_)[\d+]/g;
}

var notesVel = {}
var notesLayers = {}

var drumsNotesSet = []

var getFileNamesInFolder = function (folderPath) {
	folderPath = path.resolve(folderPath); //make it absolute.

	if (fs.lstatSync(folderPath).isDirectory()) {
		var filenames = fs.readdirSync(folderPath).sort();
		return filenames;
	} else {throw Error(`${folderPath} is not a directory!`);}
}

var cleanArray = function(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      if (parseInt(actual[i].octave) > -2) {
	      newArray.push(actual[i]);
	  }
    }
  }
  return newArray;
}

var names = getFileNamesInFolder(sfPath);
var folderName = path.basename(sfPath);

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function setNote(noteLetter) {
	
	if (!drumParsing) {

		switch (noteLetter) {
			case "A": return "10";
			case "A#": return "11";
			case "B": return "12";
			case "C": return "01";
			case "C#": return "02";
			case "D": return "03";
			case "D#": return "04";
			case "E": return "05";
			case "F": return "06";
			case "F#": return "07";
			case "G": return "08";
			case "G#": return "09";
			default:
				return false;		
		}

	} else {

		noteLetter = noteLetter.replace(/\s+/g, '').toLowerCase();
		
		switch (true) {

			case (/bd/.test(noteLetter) || /kick/.test(noteLetter) || /bass/.test(noteLetter)) && !drumsNotesSet[36]:
				drumsNotesSet[36] = true;
				return ["01", 1];
			case (/bd/.test(noteLetter) || /kick/.test(noteLetter) || /bass/.test(noteLetter)) && !drumsNotesSet[35]:
				drumsNotesSet[35] = true;
				return ["12", 0];			
			case (/sidestick/.test(noteLetter) || /rim/.test(noteLetter)) && !drumsNotesSet[37]:
				drumsNotesSet[37] = true;
				return ["02", 1];
			case (/sd/.test(noteLetter) || /snare/.test(noteLetter)) && !drumsNotesSet[38]:
				drumsNotesSet[38] = true;
				return ["03", 1];
			case (/clap/.test(noteLetter)) && !drumsNotesSet[39]:
				drumsNotesSet[39] = true;
				return ["04", 1];
			case (/sd/.test(noteLetter) || /snare/.test(noteLetter)) && !drumsNotesSet[40]:
				drumsNotesSet[40] = true;
				return ["05", 1];
			case (/tomlo/.test(noteLetter) || /tom/.test(noteLetter)) && !drumsNotesSet[41]:
				drumsNotesSet[41] = true;
				return ["06", 1];
			case (/ch/.test(noteLetter) || /hh/.test(noteLetter)) && !drumsNotesSet[42]:
				drumsNotesSet[42] = true;
				return ["07", 1];
			case (/tomhi/.test(noteLetter)) && !drumsNotesSet[43]:
				drumsNotesSet[43] = true;
				return ["08", 1];
			case (/pedalhh/.test(noteLetter) || /ch/.test(noteLetter) || /hh/.test(noteLetter)) && !drumsNotesSet[44]:
				drumsNotesSet[44] = true;
				return ["09", 1];
			case (/tomlo/.test(noteLetter)) && !drumsNotesSet[45]:
				drumsNotesSet[45] = true;
				return ["10", 1];
			case (/oh/.test(noteLetter)) && !drumsNotesSet[46]:
				drumsNotesSet[46] = true;
				return ["11", 1];
			case (/tommid/.test(noteLetter)) && !drumsNotesSet[47]:
				drumsNotesSet[47] = true;
				return ["12", 1];
			case (/tomhi/.test(noteLetter)) && !drumsNotesSet[48]:
				drumsNotesSet[48] = true;
				return ["01", 2];
			case (/crash/.test(noteLetter)) && !drumsNotesSet[49]:
				drumsNotesSet[49] = true;
				return ["02", 2];
			case (/tomhi/.test(noteLetter)) && !drumsNotesSet[50]:
				drumsNotesSet[50] = true;
				return ["03", 2];
			case (/ride/.test(noteLetter)) && !drumsNotesSet[51]:
				drumsNotesSet[51] = true;
				return ["04", 2];
			case (/rim/.test(noteLetter)) && !drumsNotesSet[52]:
				drumsNotesSet[52] = true;
				return ["05", 2];
			case (/ride/.test(noteLetter)) && !drumsNotesSet[53]:
				drumsNotesSet[53] = true;
				return ["06", 2];
			case (/tambourine/.test(noteLetter) || /tam/.test(noteLetter)) && !drumsNotesSet[54]:
				drumsNotesSet[54] = true;
				return ["07", 2];
			case (/crash/.test(noteLetter)) && !drumsNotesSet[55]:
				drumsNotesSet[55] = true;
				return ["08", 2];
			case (/cowbell/.test(noteLetter)) && !drumsNotesSet[56]:
				drumsNotesSet[56] = true;
				return ["09", 2];
			case (/crash/.test(noteLetter)) && !drumsNotesSet[57]:
				drumsNotesSet[57] = true;
				return ["10", 2];
			case (/vibraslap/.test(noteLetter)) && !drumsNotesSet[58]:
				drumsNotesSet[58] = true;
				return ["11", 2];
			case (/ride/.test(noteLetter)) && !drumsNotesSet[59]:
				drumsNotesSet[59] = true;
				return ["12", 2];
			case (/bongohi/.test(noteLetter) || /bongo/.test(noteLetter)) && !drumsNotesSet[60]:
				drumsNotesSet[60] = true;
				return ["01", 3];
			case (/bongolo/.test(noteLetter) || /bongo/.test(noteLetter)) && !drumsNotesSet[61]:
				drumsNotesSet[61] = true;
				return ["02", 3];
			case (/congahimute/.test(noteLetter) || /conga/.test(noteLetter)) && !drumsNotesSet[62]:
				drumsNotesSet[62] = true;
				return ["03", 3];
			case (/congahiopen/.test(noteLetter)) && !drumsNotesSet[63]:
				drumsNotesSet[63] = true;
				return ["04", 3];
			case (/congalo/.test(noteLetter)) && !drumsNotesSet[64]:
				drumsNotesSet[64] = true;
				return ["05", 3];
			case (/timablehi/.test(noteLetter)) && !drumsNotesSet[65]:
				drumsNotesSet[65] = true;
				return ["06", 3];
			case (/timablelo/.test(noteLetter)) && !drumsNotesSet[66]:
				drumsNotesSet[66] = true;
				return ["07", 3];
			case (/agogohi/.test(noteLetter)) && !drumsNotesSet[67]:
				drumsNotesSet[67] = true;
				return ["08", 3];
			case (/agogolo/.test(noteLetter) || /agogo/.test(noteLetter)) && !drumsNotesSet[68]:
				drumsNotesSet[68] = true;
				return ["09", 3];
			case (/cabasa/.test(noteLetter)) && !drumsNotesSet[69]:
				drumsNotesSet[69] = true;
				return ["10", 3];
			case (/maracas/.test(noteLetter)) && !drumsNotesSet[70]:
				drumsNotesSet[70] = true;
				return ["11", 3];
			case (/whistleshort/.test(noteLetter) || /whistle/.test(noteLetter)) && !drumsNotesSet[71]:
				drumsNotesSet[71] = true;
				return ["12", 3];
			case (/whistlelong/.test(noteLetter)) && !drumsNotesSet[72]:
				drumsNotesSet[72] = true;
				return ["01", 4];
			case (/guiroshort/.test(noteLetter) || /guiro/.test(noteLetter)) && !drumsNotesSet[73]:
				drumsNotesSet[73] = true;
				return ["02", 4];
			case (/guirolong/.test(noteLetter)  || /guiro/.test(noteLetter)) && !drumsNotesSet[74]:
				drumsNotesSet[74] = true;
				return ["03", 4];
			case (/claves/.test(noteLetter) || /clave/.test(noteLetter)) && !drumsNotesSet[75]:
				drumsNotesSet[75] = true;
				return ["04", 4];
			case (/woodblockhi/.test(noteLetter) || /woodblock/.test(noteLetter)) && !drumsNotesSet[76]:
				drumsNotesSet[76] = true;
				return ["05", 4];
			case (/woodblocklo/.test(noteLetter)) && !drumsNotesSet[77]:
				drumsNotesSet[77] = true;
				return ["06", 4];
			case (/cuicamute/.test(noteLetter) || /cuica/.test(noteLetter)) && !drumsNotesSet[78]:
				drumsNotesSet[78] = true;
				return ["07", 4];
			case (/cuicaopen/.test(noteLetter)) && !drumsNotesSet[79]:
				drumsNotesSet[79] = true;
				return ["08", 4];
			case (/trianglemute/.test(noteLetter) || /triangle/.test(noteLetter)) && !drumsNotesSet[80]:
				drumsNotesSet[80] = true;
				return ["09", 4];
			case (/triangleopen/.test(noteLetter) || /triangle/.test(noteLetter)) && !drumsNotesSet[81]:
				drumsNotesSet[81] = true;
				return ["10", 4];
			case (/shaker/.test(noteLetter)) && !drumsNotesSet[82]:
				drumsNotesSet[82] = true;
				return ["11", 4];
			default:

				for(o=24; o<35; o++) { 
					if (!drumsNotesSet[o]) {
						drumsNotesSet[o] = true;
						return [pad((o - 12*parseInt((o - 11 - 13)/12)-23),2), parseInt((o - 11 - 13)/12)];
					}
				}

				for(o=83; o<109; o++) { 
					if (!drumsNotesSet[o]) {
						drumsNotesSet[o] = true;
						return [pad((o - 12*parseInt((o - 11 - 13)/12)-23),2), parseInt((o - 11 - 13)/12)];
					}
				}

				return false;

		}
	}
}

var parseSFZ = function () {
	var sfz = "";
	var notes = [];
	var velocityEnabled = true;
	i = 0

	//get conversion information
	for (var f in names) {
		var note = {
			letter : "n",
			octave : {},
			note : {},
			velocity : null,
			sample : {}
		}; 

		velocityEnabled = false
		
		if (acceptExt.includes(path.extname(names[f]))) {
			// add instance
			notes[i] = note
			note.sample = names[f]; //store the filename
			
			var noteNames = names[f].match(noteExp);
			var velNums = names[f].match(velExp);
			
			if (velNums != null) { //store velocity
				note.velocity = 127; //velNums[0].replace(/\D/g,''); //isolate the velocity integers

				if (parseInt(note.velocity) > 127) { //test with a 0 in front
					console.log("\x1b[33mFile " + names[f] + " -vel is greater than 127.");
					return false// process.exit(1);
				} else {velocityEnabled = true;}
			} else if (velocityEnabled) {
				console.log("\x1b[33mCheck", names[f], "for missing velocity markings.");
				return false// process.exit(1);
			}
			
			if (noteNames == null) { //debug if dashes aren't used properly
				console.log("\x1b[33mCheck", names[f], "for improper formatting.");
				return false
			} else {

				noteNames = [noteNames[0]]

				// noteLetter = noteLetter.replace('-','')
				noteNames[0] = noteNames[0].replace('_','')//.replace('-','')

				if (noteNames[0].length > 2 && !drumParsing) { //note is sharp
					
					note.letter = (noteNames[0][0] + noteNames[0][1]).replace('-','');
					note.octave = noteNames[0].match(/-?\d+/)[0];

					note.note = setNote(note.letter);
					note.noteLayerFactor = 0

					if (!notesLayers[note.note+'_'+note.octave]) {
						notesLayers[note.note+'_'+note.octave] = 0;
					}
					notesLayers[note.note+'_'+note.octave]++;

					note.noteLayerFactor = notesLayers[note.note+'_'+note.octave]

				} else if (!drumParsing) {
					
					note.letter = noteNames[0][0];
					note.octave = noteNames[0][1];
					note.note = setNote(noteNames[0][0]);
					note.noteLayerFactor = 0

					if (!notesLayers[note.note+'_'+note.octave]) {
						notesLayers[note.note+'_'+note.octave] = 0;
					}
					notesLayers[note.note+'_'+note.octave]++;

					note.noteLayerFactor = notesLayers[note.note+'_'+note.octave]

				} else {
					note_ = setNote(names[f]);

					if (note_) {
						note.letter = note_[0];
						note.octave = note_[1];
						note.note = note_[0];
					} else {
						notes.splice(i, 1);
						console.log("\x1b[33mFile not parsed: "+note.sample);
					}	
				}
				
			}
		}

		i++		
	}

	notes = cleanArray(notes);

	notes = notes.sort(function(a,b) {

		if (a.octave == b.octave) {
			if (a.note == b.note) {			

				if (a.sample < b.sample) return -1
				else return 1

			} else { 
				return parseInt(a.note) - parseInt(b.note); 
			} //by note
		} else { return parseInt(a.octave) - parseInt(b.octave); } //by octave 
	});

	//build text file
	for (var i in notes) {
		
		if (notes[i].velocity == null) notes[i].velocity = 128;

		notesVel[notes[i].letter+'_'+notes[i].velocity+'_'+notes[i].octave] = true;		

		var down = null;
		var up = null;
		var low = "";
		var high = "";
		var vel = "";		
		
		down = findNextNote(notes[i], notes, "down");

		up = findNextNote(notes[i], notes, "up");

		if (down != null) {
			low = middleKey( notes[i], down, "down"); 
		}

		if (up != null) {
			high = middleKey( notes[i], up, "up"); 
		}
		
		sfz = sfz + "<region>\n";
		sfz = sfz + "sample=" + notes[i].sample + "\n";

		if (!drumParsing) {	

			if (low != "") {sfz = sfz + "lokey=" + notesNumber[low] + "\n";}
			if (high != "") {sfz = sfz + "hikey=" + notesNumber[high] + "\n";}

		} else if (up != null) {

			high = nameNote(notes[i], notes, notes[i].letter, notes[i].octave);

			if (high != "") {
				sfz = sfz + "lokey=" + notesNumber[high] + "\n";
				sfz = sfz + "hikey=" + notesNumber[high] + "\n";
			}

		} else if(notes[i].letter) {			
			
			sfz = sfz + "lokey=" + notesNumber[nameNote(notes[i], notes[i], parseInt(notes[i].letter), notes[i].octave)] + "\n";
			sfz = sfz + "hikey=" + notesNumber[nameNote(notes[i], notes[i], parseInt(notes[i].letter), notes[i].octave)] + "\n";
		}
		
		if (notes[i].velocity != null) {

			if (!drumParsing) {
				if (typeof notes[i-1] !== 'undefined' && notes[i-1].letter == notes[i].letter && notes[i-1].octave == notes[i].octave) {
					vel = setVelocity(notes[i-1].velocity, "high");
					sfz = sfz + "lovel=" + Math.floor(((notes[i].noteLayerFactor-1)*(127/notesLayers[notes[i].note+'_'+notes[i].octave]))) + "\n";
					sfz = sfz + "hivel=" + Math.floor(((notes[i].noteLayerFactor)*(127/notesLayers[notes[i].note+'_'+notes[i].octave]))) + "\n";
				} else {
					vel = 0;
					sfz = sfz + "lovel=" + vel + "\n";
					sfz = sfz + "hivel=" + Math.floor(((notes[i].noteLayerFactor)*(127/notesLayers[notes[i].note+'_'+notes[i].octave]))) + "\n";
				}
			} 

			if (drumParsing) {
				vel = 0;
				sfz = sfz + "lovel=" + vel + "\n";
				sfz = sfz + "hivel=127" + "\n";
			}
			
		}
		
		if (!drumParsing) {	
			sfz = sfz + "pitch_keycenter=" + notesNumber[notes[i].letter + notes[i].octave] + "\n";
		} else if (high != "") {
			sfz = sfz + "pitch_keycenter=" + notesNumber[high] + "\n";
		} else {
			sfz = sfz + "pitch_keycenter=" + notesNumber[nameNote(notes[i], notes[i], parseInt(notes[i].letter), notes[i].octave)] + "\n";
		}
		
		sfz += "\n";
		
	}
	
	return sfz;
}

function setVelocity(vel) {
	vel = parseInt(vel);
	vel = vel + 1;
	return vel.toString();
}

function findNextNote(self, friends, dir) {
	var foundSelf = false;
	
	if (dir == "up") {
		for (var f = 0; f < friends.length; f++) {
			if (foundSelf) {
				if (self.letter != friends[f].letter) {return friends[f];} 
				else if (self.octave != friends[f].octave) {return friends[f];} 
			} else if (friends[f] == self) {foundSelf = true;}	
		}
	} else { 
		for (var g = friends.length - 1; g > -1; g--) {
			if (foundSelf) {
				if (self.letter != friends[g].letter) {return friends[g];} 
				else if (self.octave != friends[g].octave) {return friends[g];} 	
			} else if (friends[g] == self) {foundSelf = true;}	
		}
	}
}

//find note between samples
function middleKey(self, friend, dir) {
	var newKey = "";
	var noteCount = [];
	
	var myNote = parseInt(self.note);
	var myOctave = parseInt(self.octave)
	var frNote = parseInt(friend.note);
	var frOctave = parseInt(friend.octave)

	var distance = distCount(myNote, frNote, myOctave, frOctave, dir);
	
	var distLow = Math.floor(distance/2);
	var distHigh = distance - distLow - 1;
	
	if (dir == "down") {noteCount = keyCount(myNote, distLow, myOctave, "down");} 
	else {noteCount = keyCount(myNote, distHigh, myOctave, "up");}
	
	newKey = nameNote(self, friend, noteCount[0], noteCount[1]);
	
	return newKey;
}

//name note...obvs
function nameNote(me, them, num, oct) {
	var numString = ""
	
	num = parseInt(num)

	switch (num) {
		case 1: numString = "C"; break;
		case 2: numString = "C#"; break;
		case 3: numString = "D"; break;
		case 4: numString = "D#"; break;
		case 5: numString = "E"; break;
		case 6: numString = "F"; break;
		case 7: numString = "F#"; break;
		case 8: numString = "G"; break;
		case 9: numString = "G#"; break;
		case 10: numString = "A"; break;
		case 11: numString = "A#"; break;
		case 12: numString = "B"; break;
	} 
	
	if (numString == them.letter && oct == them.octave) {
		numString = me.letter + me.octave;
	} else {numString = numString + oct.toString();}
	
	return numString;
}

//find note
function keyCount(countFrom, countAmount, octave, dir) {
	var count = 0;
	var newOctave = octave;
	var newKey = countFrom;
	var both = [];

	while(count < countAmount) {
		if (dir == "up") {newKey++;}
		if (dir == "down") {newKey--;}
		
		if (newKey > 12) {newOctave++; newKey = 1;}
		if (newKey < 1) {newOctave--; newKey = 12;}
		count++;
	}
	
	both.push(newKey);
	both.push(newOctave);
	
	return both;
}

//find distance
function distCount(countFrom, countTo, sOct, fOct, dir) {
	var distance = 0;
	var count = countFrom;
	var nOct = sOct;
	
	while (count != countTo || nOct != fOct) {

		if (dir == "up") {
			distance++;
			count++;	
		}
		
		if (dir == "down") {
			distance++;
			count--;
		}
		
		if (count > 12) {nOct++; count = 1;}
		if (count < 1) {nOct--; count = 12;}
	}
	
	return distance;
}

var writeFile = function (filePath, contents) {

	filePath = path.resolve(filePath);

	if (contents) {
		fs.writeFileSync(filePath, contents);
	}
}

// List all files in a directory in Node.js recursively in a synchronous fashion
var listDirs = function(dir, filelist) {
		
        var path = path || require('path');
        var fs = fs || require('fs'),
            files = fs.readdirSync(dir);
        filelist = filelist || [];
        files.forEach(function(file) {
            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist.push(path.join(dir, file));
            }

        });
        return filelist;
    };

var init = function () {

	fileList = listDirs(folderName);

	if (fileList.length) {

		data = {}

		fileList.forEach(function(file) {

			notesLayers = {}
			drumsNotesSet = []

			file = file.split('/').pop()
			names = getFileNamesInFolder(sfPath+'/'+file);

			id = md5(file)
			data[id] = parseSFZ(names)

			if (data[id]) {

				fs.writeFile(path.join(sfPath+'/'+file, file + ".sfz"), data[id], function(error){

					if (error) {
				       console.log("\x1b[0m\x1b[31m"+path.join(sfPath+'/'+file, file + ".sfz")+" wasn't written.\x1b[0m");
				     } else {
				       console.log("\x1b[0m\x1b[42m"+path.join(sfPath+'/'+file, file + ".sfz")+" written.\x1b[0m");
				     }

				})

			} else {

				console.log("\x1b[0m\x1b[31m"+path.join(sfPath+'/'+file, file + ".sfz")+" wasn't written.\x1b[0m");

			}
			

		})
	} else {

		data = parseSFZ(names)

		if (data) {

			fs.writeFile(path.join(sfPath, folderName + ".sfz"), data, function(error){

				if (error) {
			       console.log("\x1b[0m\x1b[31mNothing written.\x1b[0m");
			     } else {
			       console.log("\x1b[0m\x1b[42m"+path.join(sfPath, folderName + ".sfz")+" written.\x1b[0m");
			     }

			})

		} else {

			console.log("\x1b[0m\x1b[31mThere were errors, nothing written.\x1b[0m");

		}
	}
	
}

exports.init = init;

