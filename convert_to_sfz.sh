#!/bin/bash

find ./ -type f -name "*.exs" | while read line; do
    	
	echo "Processing file '$line'"
	
	#unlink "$line".sfz
	#exs2sfz.py "$line" "$line".sfz
	
	filename=$(basename "$line" .exs)
	
	#echo "${line%/*}/$filename.sfz"

	exs2sfz.py "$line" "${line%/*}/$filename.sfz"
	
	
done
